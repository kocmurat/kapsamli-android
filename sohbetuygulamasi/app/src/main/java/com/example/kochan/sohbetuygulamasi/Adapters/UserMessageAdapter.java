package com.example.kochan.sohbetuygulamasi.Adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.kochan.sohbetuygulamasi.Activity.ChatActivity;
import com.example.kochan.sohbetuygulamasi.Fragments.ChatFragment;
import com.example.kochan.sohbetuygulamasi.Fragments.OtherProfileFragment;
import com.example.kochan.sohbetuygulamasi.Models.Kullanicilar;
import com.example.kochan.sohbetuygulamasi.Models.MessageModel;
import com.example.kochan.sohbetuygulamasi.R;
import com.example.kochan.sohbetuygulamasi.Utils.ChangeFragment;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class UserMessageAdapter extends RecyclerView.Adapter<UserMessageAdapter.ViewHolder> {

    List<String> userKeysList;
    Activity activity;
    Context context;
    FirebaseDatabase firebaseDatabase;
    DatabaseReference reference;
    FirebaseAuth auth;
    FirebaseUser firebaseUser;
    List<String> userNameList;

    public UserMessageAdapter(List<String> userKeysList, Activity activity, Context context) {
        this.userKeysList = userKeysList;
        this.activity = activity;
        this.context = context;
        firebaseDatabase = FirebaseDatabase.getInstance();
        reference = firebaseDatabase.getReference();
        auth = FirebaseAuth.getInstance();
        firebaseUser = auth.getCurrentUser();
        userNameList = new ArrayList<>();
    }

    // layout tanımlaması yapılacak
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.user_message_layout, parent, false);
        return new ViewHolder(view);
    }

    //viewlara setlemeler yapılacak
    @Override
    public void onBindViewHolder(final @NonNull ViewHolder holder, final int position) {


        reference.child("Kullanicilar").child(userKeysList.get(position).toString()).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Kullanicilar kl = dataSnapshot.getValue(Kullanicilar.class);


                Picasso.get().load(kl.getResim()).into(holder.user_message_image);
                holder.user_message_name.setText(kl.getIsim());
                userNameList.add(kl.getIsim());


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        reference.child("Mesajlar").child(firebaseUser.getUid()).child(userKeysList.get(position)).addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                MessageModel messageModel = dataSnapshot.getValue(MessageModel.class);

                if (messageModel.getFrom().equals(firebaseUser.getUid())) {
                    holder.user_message_text.setText(messageModel.getText() + "  ✓✓ ");

                } else {
                    holder.user_message_text.setText(messageModel.getText());
                    Log.i("mesajım", messageModel.getText());
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        holder.message_Linear_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
              /*
                Intent intent = new Intent(activity,ChatActivity.class);
                intent.putExtra("userName",userNameList.get(position));
                intent.putExtra("id",userKeysList.get(position));
                activity.startActivity(intent);
*/
                ChangeFragment changeFragment = new ChangeFragment(context);
                changeFragment.changeWithParameter(new ChatFragment(),userNameList.get(position),userKeysList.get(position));

            }
        });

    }

    // adapteri oluşturulacak olan listenın size
    @Override
    public int getItemCount() {
        return userKeysList.size();
    }


    // viewların tanımlanma işlemleri yapılacak
    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView user_message_name, user_message_text;
        CircleImageView user_message_image;
        LinearLayout message_Linear_layout;

        ViewHolder(View itemView) {
            super(itemView);
            user_message_name = (TextView) itemView.findViewById(R.id.user_message_name);
            user_message_text = (TextView) itemView.findViewById(R.id.user_message_text);
            user_message_image = (CircleImageView) itemView.findViewById(R.id.user_message_image);
            message_Linear_layout = (LinearLayout) itemView.findViewById(R.id.message_Linear_layout);


        }
    }

}
