package com.example.kochan.sohbetuygulamasi.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import com.example.kochan.sohbetuygulamasi.Fragments.AnaSayfaFragment;
import com.example.kochan.sohbetuygulamasi.Fragments.BildirimFragment;
import com.example.kochan.sohbetuygulamasi.Fragments.KullaniciProfilFragment;
import com.example.kochan.sohbetuygulamasi.Fragments.MessageFragment;
import com.example.kochan.sohbetuygulamasi.Utils.ChangeFragment;
import com.example.kochan.sohbetuygulamasi.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class AnaActivity extends AppCompatActivity {


    private ChangeFragment changeFragment;
    private FirebaseAuth auth;
    private FirebaseUser user;


    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    changeFragment.change(new AnaSayfaFragment());
                    return true;
                case R.id.navigation_dashboard:
                    changeFragment.change(new MessageFragment());
                    return true;
                case R.id.navigation_profil:
                    changeFragment.change(new KullaniciProfilFragment());
                    return true;
                case R.id.navigation_exit:
                    cik();
                    return true;
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        tanimla();
        kontrol();

        changeFragment = new ChangeFragment(AnaActivity.this);
        changeFragment.change(new AnaSayfaFragment());

        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
    }

    public void cik()
    {
        auth.signOut();
        Intent ıntent = new Intent(AnaActivity.this , GirisActivity.class);
        startActivity(ıntent);
        finish();
        FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
        DatabaseReference reference = firebaseDatabase.getReference().child("Kullanicilar");
        reference.child(user.getUid()).child("state").setValue(false);
    }

    public void tanimla()
    {
        auth = FirebaseAuth.getInstance();
        user = auth.getCurrentUser();


    }

    @Override
    protected void onStop() {
        super.onStop();
        FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
        DatabaseReference reference = firebaseDatabase.getReference().child("Kullanicilar");
        reference.child(user.getUid()).child("state").setValue(false);

    }

    @Override
    protected void onResume() {
        super.onResume();

        FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
        DatabaseReference reference = firebaseDatabase.getReference().child("Kullanicilar");
        reference.child(user.getUid()).child("state").setValue(true);
    }

    public void kontrol()
    {
        if(user==null)
        {
            Intent ıntent = new Intent(AnaActivity.this , GirisActivity.class);
            startActivity(ıntent);
            finish();

        }else
        {
            FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
            DatabaseReference reference = firebaseDatabase.getReference().child("Kullanicilar");
            reference.child(user.getUid()).child("state").setValue(true);
        }
    }


}
