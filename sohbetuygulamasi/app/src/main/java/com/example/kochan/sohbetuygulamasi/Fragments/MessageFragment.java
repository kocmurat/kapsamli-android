package com.example.kochan.sohbetuygulamasi.Fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.kochan.sohbetuygulamasi.Adapters.UserMessageAdapter;
import com.example.kochan.sohbetuygulamasi.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.List;

public class MessageFragment extends Fragment {


    FirebaseAuth auth;
    FirebaseUser firebaseUser;
    FirebaseDatabase firebaseDatabase;
    DatabaseReference reference;
    String userId;
    RecyclerView user_meesage_recy;
    List<String> userKeyList;
    UserMessageAdapter userMessageAdapter;
    View view;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        view =  inflater.inflate(R.layout.fragment_message, container, false);
        tanimla();
        getMessage(userId);
        return  view;
    }

    public void tanimla()
    {
        userKeyList = new ArrayList<>();
        auth = FirebaseAuth.getInstance();
        firebaseUser = auth.getCurrentUser();
        userId = firebaseUser.getUid();
        firebaseDatabase = FirebaseDatabase.getInstance();
        reference = firebaseDatabase.getReference();

        user_meesage_recy =(RecyclerView) view.findViewById(R.id.user_meesage_recy);
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(getContext(),1);
        user_meesage_recy.setLayoutManager(layoutManager);
        userMessageAdapter = new UserMessageAdapter(userKeyList,getActivity(),getContext());

        user_meesage_recy.setAdapter(userMessageAdapter);
    }

    public void getMessage(String userId)
    {
        reference.child("Mesajlar").child(userId).addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {

                userKeyList.add(dataSnapshot.getKey());
                userMessageAdapter.notifyDataSetChanged();
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }
}
