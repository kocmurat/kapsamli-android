package com.example.kochan.sohbetuygulamasi.Fragments;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.kochan.sohbetuygulamasi.Activity.ChatActivity;
import com.example.kochan.sohbetuygulamasi.Models.Kullanicilar;
import com.example.kochan.sohbetuygulamasi.R;
import com.example.kochan.sohbetuygulamasi.Utils.ChangeFragment;
import com.example.kochan.sohbetuygulamasi.Utils.ShowToastMessage;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;


public class OtherProfileFragment extends Fragment {


    View view;
    String otherId, userId;
    TextView userProfileNameText, userProfileEgitimText, userProfileDogumText,
            userProfileHakkımdaText, userProfileTakipText, userProfileArkadasText, userProfileNameText2;

    ImageView userProfileArkadasImage, userProfileMesajImage, userProfileTakipImage;
    FirebaseDatabase firebaseDatabase;
    DatabaseReference reference, referance_Arkadaslik;
    CircleImageView userProfileProfileImage;
    FirebaseAuth auth;
    FirebaseUser user;
    String kontrol = "", begeniKontrol = "";
    ShowToastMessage showToastMessage;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_user_profile, container, false);
        tanimla();
        action();
        getBegeniText();
        getArkadasText();
        return view;

    }

    public void tanimla() {

        firebaseDatabase = FirebaseDatabase.getInstance();
        reference = firebaseDatabase.getReference();
        referance_Arkadaslik = firebaseDatabase.getReference().child("Arkadaslik_Istek");
        auth = FirebaseAuth.getInstance();
        user = auth.getCurrentUser();
        userId = user.getUid();

        otherId = getArguments().getString("userid");


        userProfileTakipText = (TextView) view.findViewById(R.id.userProfileTakipText);
        userProfileArkadasText = (TextView) view.findViewById(R.id.userProfileArkadasText);

        userProfileNameText = (TextView) view.findViewById(R.id.userProfileNameText);
        userProfileEgitimText = (TextView) view.findViewById(R.id.userProfileEgitimText);
        userProfileDogumText = (TextView) view.findViewById(R.id.userProfileDogumText);
        userProfileHakkımdaText = (TextView) view.findViewById(R.id.userProfileHakkımdaText);
        userProfileNameText2 = (TextView) view.findViewById(R.id.userProfileNameText2);
        userProfileArkadasImage = (ImageView) view.findViewById(R.id.userProfileArkadasImage);
        userProfileMesajImage = (ImageView) view.findViewById(R.id.userProfileMesajImage);
        userProfileTakipImage = (ImageView) view.findViewById(R.id.userProfileTakipImage);
        userProfileProfileImage = (CircleImageView) view.findViewById(R.id.userProfileProfileImage);

        referance_Arkadaslik.child(otherId).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.hasChild(userId)) {
                    kontrol = "istek";

                    userProfileArkadasImage.setImageResource(R.drawable.arkadas_ekle_off);
                } else {

                    userProfileArkadasImage.setImageResource(R.drawable.arkadas_ekle_on);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        reference.child("Arkadaslar").child(userId).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.hasChild(otherId)) {
                    kontrol = "arkadas";
                    userProfileArkadasImage.setImageResource(R.drawable.deletinguser);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        reference.child("Begeniler").child(otherId).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.hasChild(userId)) {
                    begeniKontrol = "begendi";
                    userProfileTakipImage.setImageResource(R.drawable.takip_ok);
                } else {
                    userProfileTakipImage.setImageResource(R.drawable.takip_off);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        showToastMessage = new ShowToastMessage(getContext());

    }

    public void action() {
        reference.child("Kullanicilar").child(otherId).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Kullanicilar kl = dataSnapshot.getValue(Kullanicilar.class);
                userProfileNameText.setText("İsim : " + kl.getIsim());
                userProfileEgitimText.setText("Eğitim : " + kl.getEgitim());
                userProfileDogumText.setText("Doğum Tarihi : " + kl.getDogumtarih());
                userProfileHakkımdaText.setText("Hakkımda : " + kl.getHakkimda());
                userProfileNameText2.setText(kl.getIsim());
                if (!kl.getResim().equals("null")) {
                    Picasso.get().load(kl.getResim()).into(userProfileProfileImage);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        userProfileArkadasImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (kontrol.equals("istek")) {

                    arkadasIptalEt(otherId, userId);

                } else if (kontrol.equals("arkadas")) {
                    arkadasTablosundanCikar(otherId, userId);
                } else {
                    arkadasEkle(otherId, userId);
                }

            }
        });

        userProfileTakipImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (begeniKontrol.equals("begendi")) {
                    begeniIptal(userId, otherId);
                } else {
                    begen(userId, otherId);
                }
            }
        });

        userProfileMesajImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                /*
                Intent intent = new Intent(getActivity(),ChatActivity.class);
                intent.putExtra("userName",userProfileNameText2.getText().toString());
                intent.putExtra("id",otherId);
                startActivity(intent);
*/
                ChangeFragment changeFragment = new ChangeFragment(getContext());
                changeFragment.changeWithParameter(new ChatFragment(),userProfileNameText2.getText().toString(),otherId);

            }
        });
    }

    private void begeniIptal(String userId, String otherId) {

        reference.child("Begeniler").child(otherId).child(userId).removeValue().addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                userProfileTakipImage.setImageResource(R.drawable.takip_off);
                begeniKontrol = "";
                showToastMessage.showToast("Beğenme Iptal Edildi");
                getBegeniText();
            }
        });
    }

    private void arkadasTablosundanCikar(final String otherId, final String userId) {

        reference.child("Arkadaslar").child(otherId).child(userId).removeValue().addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                reference.child("Arkadaslar").child(userId).child(otherId).removeValue().addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        kontrol = "";

                        userProfileArkadasImage.setImageResource(R.drawable.arkadas_ekle_on);
                        showToastMessage.showToast("Arkadaşlıktan çıkarıldı");
                        getArkadasText();

                    }
                });
            }
        });
    }

    public void arkadasEkle(final String otherId, final String userId) {

        referance_Arkadaslik.child(userId).child(otherId).child("tip").setValue("gonderdi")
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            referance_Arkadaslik.child(otherId).child(userId).child("tip").setValue("aldi")
                                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {
                                            if (task.isSuccessful()) {
                                                kontrol = "istek";
                                                Toast.makeText(getContext(), "Arkadaşlık İsteği Başarıyla Gönderildi.", Toast.LENGTH_LONG).show();
                                                userProfileArkadasImage.setImageResource(R.drawable.arkadas_ekle_off);

                                            } else {
                                                showToastMessage.showToast("Bir problem meydana geldi");
                                            }
                                        }
                                    });
                        } else {
                            showToastMessage.showToast("Bir problem meydana geldi");
                        }
                    }
                });
    }

    public void arkadasIptalEt(final String otherId, final String userId) {
        referance_Arkadaslik.child(otherId).child(userId).removeValue().addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                referance_Arkadaslik.child(userId).child(otherId).removeValue().addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        kontrol = "";

                        userProfileArkadasImage.setImageResource(R.drawable.arkadas_ekle_on);
                        showToastMessage.showToast("Arkadaşlık İsteği İptal Edildi.");


                    }
                });
            }
        });
    }

    public void begen(String userId, String otherId) {

        reference.child("Begeniler").child(otherId).child(userId).child("tip").setValue("begendi").addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    showToastMessage.showToast("Profili Beğendiniz");

                    userProfileTakipImage.setImageResource(R.drawable.takip_ok);
                    begeniKontrol = "begendi";
                    getBegeniText();
                }
            }
        });

    }

    public void getBegeniText() {

        //final List<String> begeniList = new ArrayList<>();
        // userProfileTakipText.setText("0 Beğeni");

        reference.child("Begeniler").child(otherId).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                userProfileTakipText.setText(dataSnapshot.getChildrenCount() + " Beğeni");
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public void getArkadasText() {
        // getChildrencount için addListenerForSingleValueEvent kullanılması önerilir.
        //final List<String> arkList = new ArrayList<>();
        // userProfileArkadasText.setText("0 Arkadaş");

        reference.child("Arkadaslar").child(otherId).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                userProfileArkadasText.setText(dataSnapshot.getChildrenCount() + " Arkadaş");
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
}
