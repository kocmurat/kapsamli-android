package com.example.kochan.sohbetuygulamasi.Activity;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.kochan.sohbetuygulamasi.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class GirisActivity extends AppCompatActivity {

    private EditText input_email_login, input_password_login;
    private Button loginButon;
    private FirebaseAuth auth;
    private TextView hesapYok;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_giris);
        tanimla();
    }

    public void tanimla() {
        input_email_login = (EditText) findViewById(R.id.input_email_login);
        input_password_login = (EditText) findViewById(R.id.input_password_login);
        loginButon = (Button) findViewById(R.id.loginButon);
        hesapYok = (TextView) findViewById(R.id.hesapYok);
        auth = FirebaseAuth.getInstance();
        loginButon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String email = input_email_login.getText().toString();
                String pass = input_password_login.getText().toString();
                if (!email.equals("") && !pass.equals("")) {
                    sistemeGiris(email, pass);
                } else {
                    Toast.makeText(getApplicationContext(), "Boş girilemez...", Toast.LENGTH_LONG).show();
                }
            }
        });
        hesapYok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent ıntent = new Intent(GirisActivity.this, KayitOlActivity.class);
                startActivity(ıntent);
                finish();
            }
        });
    }

    public void sistemeGiris(String email, String pass) {
        auth.signInWithEmailAndPassword(email, pass).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) {
                    Intent ıntent = new Intent(GirisActivity.this, AnaActivity.class);
                    startActivity(ıntent);
                    finish();

                } else {
                    Toast.makeText(getApplicationContext(), "Hatalı bilgi girdiniz...", Toast.LENGTH_LONG).show();

                }
            }
        });
    }
}
