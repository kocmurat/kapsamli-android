package com.example.mk.alertdialog;

import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    Button button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        tanimla();

    }

    public void tanimla() {
        button = (Button) findViewById(R.id.dialogAc);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogAc();

            }
        });

    }

    public void dialogAc() {
        LayoutInflater ınflater = this.getLayoutInflater();
        View view = ınflater.inflate(R.layout.alertlayout, null);

        final EditText mailEditText = (EditText) view.findViewById(R.id.mailAdres);
        final EditText kadiEditText = (EditText) view.findViewById(R.id.kullanici);
        final EditText sifreEditText = (EditText) view.findViewById(R.id.sifre);

        Button button = (Button)view.findViewById(R.id.buton);
        Button buttonCık = (Button)view.findViewById(R.id.buton2);

        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setView(view);
        alert.setCancelable(false);
        final AlertDialog dialog = alert.create();
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(), mailEditText.getText().toString() + " "
                                + kadiEditText.getText().toString() + " " + sifreEditText.getText().toString()
                        , Toast.LENGTH_LONG).show();
                dialog.cancel();
            }
        });
        buttonCık.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
            }
        });

        dialog.show();

    }

}
