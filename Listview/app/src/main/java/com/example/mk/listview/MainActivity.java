package com.example.mk.listview;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    List<KullaniciModel> kullaniciList;
    KulliniciListAdapter adp;
    ListView listView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        tanimla();
        listeDoldur();
    }


    public void listeDoldur()
    {
        kullaniciList = new ArrayList<>();
        KullaniciModel k1 = new KullaniciModel("murat","22","koç","x");
        KullaniciModel k2 = new KullaniciModel("ibrahim","51","koç","k");
        KullaniciModel k3 = new KullaniciModel("başar","19","koç","t");
        KullaniciModel k4 = new KullaniciModel("ahmet","22","kavak","y");
        kullaniciList.add(k1);
        kullaniciList.add(k2);
        kullaniciList.add(k3);
        kullaniciList.add(k4);



        adp = new KulliniciListAdapter(kullaniciList,this);

        listView.setAdapter(adp);



    }
    void tanimla()
    {
        listView = (ListView) findViewById(R.id.listView);
    }
}
