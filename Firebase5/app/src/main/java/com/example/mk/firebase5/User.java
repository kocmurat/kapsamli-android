package com.example.mk.firebase5;

/**
 * Created by mk on 25.03.2018.
 */

public class User {

    private static String userName;

    public static String getUserName() {
        return userName;
    }

    public static void setUserName(String userName) {
        User.userName = userName;
    }
}
