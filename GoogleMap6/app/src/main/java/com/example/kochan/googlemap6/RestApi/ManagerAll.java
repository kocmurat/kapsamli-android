package com.example.kochan.googlemap6.RestApi;





import com.example.kochan.googlemap6.Models.AnaPojo;

import retrofit2.Call;

public class ManagerAll extends BaseManager {


    private static ManagerAll ourInstance = new ManagerAll();

    public static synchronized ManagerAll getInstance() {
        return ourInstance;
    }

    public Call<AnaPojo> getCity(String lat) {
        Call<AnaPojo> x = getRestApi().getInformation(lat);
        return x;
    }

}
