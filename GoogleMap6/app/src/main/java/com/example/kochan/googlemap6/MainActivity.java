package com.example.kochan.googlemap6;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import com.example.kochan.googlemap6.Models.AnaPojo;
import com.example.kochan.googlemap6.RestApi.ManagerAll;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    TextView cadde,mahalle,ilce,il,ulke,posta;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        cadde = findViewById(R.id.cadde);
        mahalle = findViewById(R.id.mahalle);
        ilce = findViewById(R.id.ilce);
        il = findViewById(R.id.il);
        ulke = findViewById(R.id.ulke);
        posta = findViewById(R.id.posta);

       getir();

    }

    public void getir()
    {
        Call<AnaPojo> req = ManagerAll.getInstance().getCity("40.99121,29.019149");
        req.enqueue(new Callback<AnaPojo>() {
            @Override
            public void onResponse(Call<AnaPojo> call, Response<AnaPojo> response) {
                if(response.isSuccessful())
                {
                    
                    cadde.setText(response.body().getResults().get(0).getAddressComponents().get(0).getLongName());
                    mahalle.setText(response.body().getResults().get(0).getAddressComponents().get(1).getLongName());
                    ilce.setText(response.body().getResults().get(0).getAddressComponents().get(2).getLongName());
                    il.setText(response.body().getResults().get(0).getAddressComponents().get(3).getLongName());
                    ulke.setText(response.body().getResults().get(0).getAddressComponents().get(4).getLongName());
                    posta.setText(response.body().getResults().get(0).getAddressComponents().get(5).getLongName());

                }

            }

            @Override
            public void onFailure(Call<AnaPojo> call, Throwable t) {
                Log.i("cevappp",""+t);
            }
        });
    }
}
