package com.example.mk.intentkavramveaktivitylerarasgei;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class Activity2 extends AppCompatActivity {

    TextView txt;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity2);
        tanimla();
    }
    public void gec()
    {
        Intent ıntent = new Intent(this,MainActivity.class);
        startActivity(ıntent);
    }

    public void tanimla()
    {
        txt=(TextView)findViewById(R.id.text);
        txt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               gec();
            }
        });
    }
}
