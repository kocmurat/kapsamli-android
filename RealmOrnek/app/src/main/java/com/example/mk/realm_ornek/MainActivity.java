package com.example.mk.realm_ornek;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.data.BarData;

import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;


import java.util.ArrayList;

import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;

public class MainActivity extends AppCompatActivity {

    Realm realm;
    EditText buyukTansiyonEditText, kucukTansiyonEditText;
    Button ekleButon;
    BarChart chart;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        realm = Realm.getDefaultInstance();
        tanimla();
        ekle();
        goster();
        listele();
       // sil();
    }

    public void tanimla() {
        buyukTansiyonEditText = (EditText) findViewById(R.id.buyukTansiyon);
        kucukTansiyonEditText = (EditText) findViewById(R.id.kucukTansiyon);
        ekleButon = (Button) findViewById(R.id.ekleButon);
        chart = (BarChart) findViewById(R.id.barChart);

    }

    public void ekle() {
        ekleButon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        tansiyon tns = realm.createObject(tansiyon.class);
                        tns.setBuyukTansiyon(buyukTansiyonEditText.getText().toString());
                        tns.setKucukTansiyon(kucukTansiyonEditText.getText().toString());
                    }

                });
                listele();
                goster();
            }

        });
    }

    public void listele() {
        RealmResults<tansiyon> list = realm.where(tansiyon.class).findAll();
        for (tansiyon t : list) {
            Log.i("ekleme", t.toString());
        }

    }

    public void goster()
    {
        RealmResults<tansiyon> kisi = realm.where(tansiyon.class).findAll();
        Float buyukTansiyon=0.f;
        Float kucukTansiyon=0.f;
        for ( int i = 0 ; i < kisi.size(); i ++)
        {

            buyukTansiyon = buyukTansiyon + Float.parseFloat(kisi.get(i).getBuyukTansiyon());
            kucukTansiyon = kucukTansiyon + Float.parseFloat(kisi.get(i).getKucukTansiyon());
        }

        ArrayList<BarEntry> arrayList = new ArrayList<>();
        arrayList.add(new BarEntry(buyukTansiyon,0));
        arrayList.add(new BarEntry(kucukTansiyon,1));
        BarDataSet barDataSet = new BarDataSet(arrayList,"Toplam Değer");

        ArrayList<String > sutunIsmi = new ArrayList<>();
        sutunIsmi.add("Büyük Tansiyon");
        sutunIsmi.add("Küçük Tansiyon");

        BarData barData = new BarData(sutunIsmi,barDataSet);
        chart.setData(barData);
        chart.setDescription("Tansiyon Değerlerini Gösteren Grafik Arayüzüdür...");

        chart.invalidate();


    }

    public void sil()
    {
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                RealmResults<tansiyon> kisi = realm.where(tansiyon.class).findAll();
                kisi.deleteFromRealm(1);

            }
        });
    }


}
