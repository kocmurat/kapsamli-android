package com.example.mk.realm_ornek;

import io.realm.RealmObject;
import io.realm.annotations.RealmClass;

/**
 * Created by mk on 24.12.2017.
 */
@RealmClass
public class tansiyon  extends RealmObject{

    private String buyukTansiyon;
    private String kucukTansiyon ;

    public String getBuyukTansiyon() {
        return buyukTansiyon;
    }

    public void setBuyukTansiyon(String buyukTansiyon) {
        this.buyukTansiyon = buyukTansiyon;
    }

    public String getKucukTansiyon() {
        return kucukTansiyon;
    }

    public void setKucukTansiyon(String kucukTansiyon) {
        this.kucukTansiyon = kucukTansiyon;
    }

    @Override
    public String toString() {
        return "tansiyon{" +
                "buyukTansiyon='" + buyukTansiyon + '\'' +
                ", kucukTansiyon='" + kucukTansiyon + '\'' +
                '}';
    }
}
