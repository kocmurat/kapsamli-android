package com.example.mk.htmlparsing;


import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.io.InputStream;


public class MainActivity extends AppCompatActivity {

    private static final String url = "http://muratkoc93.xyz/htmlparsing.html";
    Document document;
    TextView textView, h1TextView, pTextView;
    String h1Elementi, imageUrl;
    Bitmap bitmap;

    Elements h1Elemet, srcElemet, pElemet, liElemets;
    ;
    ImageView img;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        new getirHtml().execute();

    }


    public class getirHtml extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            textView = (TextView) findViewById(R.id.textView);
            h1TextView = (TextView) findViewById(R.id.h1TextView);
            pTextView = (TextView) findViewById(R.id.pTextView);
            img = (ImageView) findViewById(R.id.resim);
        }

        @Override
        protected Void doInBackground(Void... voids) {
            try {
                document = Jsoup.connect(url).get();
                h1Elemet = document.select("h1");
                pElemet = document.select("p");
                srcElemet = document.select("img");
                imageUrl = srcElemet.attr("src");
                //InputStream input = new java.net.URL(imageUrl).openStream();
                // bitmap = BitmapFactory.decodeStream(input);
                liElemets = document.select("ul>li");
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            Log.i("kaynak", "" + liElemets);
            textView.setText(document.title());
            h1Elementi = h1Elemet.text();
            h1TextView.setText(h1Elementi);
            pTextView.setText(pElemet.text());
            //  img.setImageBitmap(bitmap);
            Picasso.with(getApplicationContext()).load(imageUrl).into(img);
            for (int i = 0; i < liElemets.size(); i++) {
               Log.i("li elementleri",i+". li elementi = " + liElemets.get(i).text());
            }
        }
    }


}
