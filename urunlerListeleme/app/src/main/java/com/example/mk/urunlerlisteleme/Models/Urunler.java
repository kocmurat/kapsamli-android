package com.example.mk.urunlerlisteleme.Models;

public class Urunler{

	private String urunadi;
	private String urunresmi;
	private String urunfiyati;
	private String id;

	public void setUrunadi(String urunadi){
		this.urunadi = urunadi;
	}

	public String getUrunadi(){
		return urunadi;
	}

	public void setUrunresmi(String urunresmi){
		this.urunresmi = urunresmi;
	}

	public String getUrunresmi(){
		return urunresmi;
	}

	public void setUrunfiyati(String urunfiyati){
		this.urunfiyati = urunfiyati;
	}

	public String getUrunfiyati(){
		return urunfiyati;
	}

	public void setId(String id){
		this.id = id;
	}

	public String getId(){
		return id;
	}

	@Override
 	public String toString(){
		return 
			"Urunler{" + 
			"urunadi = '" + urunadi + '\'' + 
			",urunresmi = '" + urunresmi + '\'' + 
			",urunfiyati = '" + urunfiyati + '\'' + 
			",id = '" + id + '\'' + 
			"}";
		}
}
