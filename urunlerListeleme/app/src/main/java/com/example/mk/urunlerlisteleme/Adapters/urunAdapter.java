package com.example.mk.urunlerlisteleme.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.mk.urunlerlisteleme.Models.Urunler;
import com.example.mk.urunlerlisteleme.R;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by mk on 20.01.2018.
 */

public class urunAdapter extends BaseAdapter {

    public List<Urunler> urunList;
    public Context context;

    public urunAdapter(List<Urunler> urunList, Context context) {
        this.urunList = urunList;
        this.context = context;
    }

    @Override
    public int getCount() {
        return urunList.size();
    }

    @Override
    public Object getItem(int position) {
        return urunList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = LayoutInflater.from(context).inflate(R.layout.layout, parent, false);
        ImageView urunResmi;
        TextView urunId, urunIsmi, urunFiyati;
        urunResmi = (ImageView) convertView.findViewById(R.id.urunResmi);
        urunId = (TextView) convertView.findViewById(R.id.urunİd);
        urunFiyati = (TextView) convertView.findViewById(R.id.urunFiyati);
        urunIsmi = (TextView) convertView.findViewById(R.id.urunIsmi);

        urunFiyati.setText("Urun Fiyatı : " + urunList.get(position).getUrunfiyati());
        urunId.setText("Urun İd : "+ urunList.get(position).getId());
        urunIsmi.setText("Urun Ismi : "+urunList.get(position).getUrunadi());

        Picasso.with(context).load("https://muratkoc93.xyz/resimler/"+urunList.get(position).getUrunresmi()).into(urunResmi);





        return convertView;
    }
}
