package com.example.mk.urunlerlisteleme.RestApi;


import com.example.mk.urunlerlisteleme.Models.Urunler;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

/**
 * Created by mk on 07.01.2018.
 */

public interface RestApi {

    @GET("/urunler.php")
    Call<List<Urunler>> listele();




}
