package com.example.mk.mock;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Adapter;
import android.widget.ListAdapter;
import android.widget.ListView;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    ListView listview;
    BilgiAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        x();
    }

    public void x() {

        listview = (ListView) findViewById(R.id.listview);


        Call<List<Test>> c = ManagerAll.getInstanse().getirBilgi();
        c.enqueue(new Callback<List<Test>>() {
            @Override
            public void onResponse(Call<List<Test>> call, Response<List<Test>> response) {
                Log.i("ahmet", response.body().toString());
                if (response.isSuccessful()) {
                    adapter = new BilgiAdapter(response.body(), MainActivity.this);

                    listview.setAdapter(adapter);
                }
            }

            @Override
            public void onFailure(Call<List<Test>> call, Throwable t) {

            }
        });

    }


}
