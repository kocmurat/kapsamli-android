package com.example.mk.mock;



import java.util.List;

import retrofit2.Call;

/**
 * Created by mk on 02.01.2018.
 */

public class ManagerAll extends BaseManager{

    private static  ManagerAll ourgetInstanse = new ManagerAll();

    public  static synchronized  ManagerAll getInstanse()
    {
        return ourgetInstanse;
    }

    public Call<List<Test>> getirBilgi()
    {
        Call<List<Test>> x = getRestpiClient().getir();
        return x;
    }
}
