package com.example.mk.webservis;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ListView;

import com.example.mk.webservis.Models.Bilgiler;
import com.example.mk.webservis.RestApi.ManagerAll;
import com.example.mk.webservis.adapters.adapterbilgi;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {
List<Bilgiler> list ;
ListView listView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        tanimla();
        istek();
    }

    public  void tanimla()
    {
        listView = (ListView)findViewById(R.id.list_view);
    }

    public void istek()
    {
        list = new ArrayList<>();

        Call<List<Bilgiler>>  bilgiList = ManagerAll.getInstance().getirBilgileri();
        bilgiList.enqueue(new Callback<List<Bilgiler>>() {
            @Override
            public void onResponse(Call<List<Bilgiler>> call, Response<List<Bilgiler>> response) {
               if( response.isSuccessful())
               {
                   list = response.body();
                   adapterbilgi  adp = new adapterbilgi(list,getApplicationContext());
                   listView.setAdapter(adp);
               }


            }

            @Override
            public void onFailure(Call<List<Bilgiler>> call, Throwable t) {

            }
        });
    }
}
