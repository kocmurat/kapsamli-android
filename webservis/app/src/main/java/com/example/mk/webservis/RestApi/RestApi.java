package com.example.mk.webservis.RestApi;

import com.example.mk.webservis.Models.Bilgiler;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by mk on 30.12.2017.
 */

public interface RestApi {
    @GET("/posts")
    Call<List<Bilgiler>> bilgiGetir();

}
