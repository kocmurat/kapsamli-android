package com.example.mk.webservis_2;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ListView;


import com.example.mk.webservis_2.Adapters.BilgiAdapter;
import com.example.mk.webservis_2.Models.Bilgi;
import com.example.mk.webservis_2.RestApi.ManagerAll;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    List<Bilgi> bilgiList;
    ListView listView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        tanimla();
        istek();

    }
    public void tanimla()
    {
        listView = (ListView)findViewById(R.id.list_view);
    }

    public void istek()
    {
        bilgiList = new ArrayList<>();
        Call<List<Bilgi>> listCall = ManagerAll.getInstanse().getirBilgi();
        listCall.enqueue(new Callback<List<Bilgi>>() {
            @Override
            public void onResponse(Call<List<Bilgi>> call, Response<List<Bilgi>> response) {
                if(response.isSuccessful())
                {
                    bilgiList =  response.body();
                    BilgiAdapter bilgiAdapter = new BilgiAdapter(bilgiList,getApplicationContext());
                    listView.setAdapter(bilgiAdapter);
                }

            }

            @Override
            public void onFailure(Call<List<Bilgi>> call, Throwable t) {

            }
        });
    }




}
