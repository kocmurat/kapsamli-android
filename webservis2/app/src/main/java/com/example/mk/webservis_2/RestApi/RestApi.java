package com.example.mk.webservis_2.RestApi;

import com.example.mk.webservis_2.Models.Bilgi;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by mk on 02.01.2018.
 */

public interface RestApi {
    @GET("/todos")
    Call<List<Bilgi>>  getir();


}
