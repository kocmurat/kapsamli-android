package com.example.mk.webservis_3.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.mk.webservis_3.Models.Bilgi;
import com.example.mk.webservis_3.R;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by mk on 03.01.2018.
 */

public class BilgiAdapter extends BaseAdapter {

    List<Bilgi> list;
    Context context;

    public BilgiAdapter(List<Bilgi> list, Context context) {
        this.list = list;
        this.context = context;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = LayoutInflater.from(context).inflate(R.layout.layout, parent, false);
        TextView albumId, id, title, url;
        albumId = (TextView) convertView.findViewById(R.id.albumId);
        id = (TextView) convertView.findViewById(R.id.id);
        title = (TextView) convertView.findViewById(R.id.title);
        url = (TextView) convertView.findViewById(R.id.url);
        ImageView ımageView = (ImageView) convertView.findViewById(R.id.thumbnailUrl);
        albumId.setText(""+list.get(position).getAlbumId());
        id.setText(""+list.get(position).getId());
        title.setText(list.get(position).getTitle());
        url.setText(list.get(position).getUrl());
        Picasso.with(context).load(list.get(position).getThumbnailUrl()).into(ımageView);
        return convertView;
    }
}
