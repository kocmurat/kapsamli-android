package com.example.mk.webservis_3.RestApi;

import com.example.mk.webservis_3.Models.Bilgi;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by mk on 03.01.2018.
 */

public interface RestApi {
    @GET("/photos")
    Call<List<Bilgi>> getir();
}
