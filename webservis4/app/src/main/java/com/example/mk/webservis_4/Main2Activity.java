package com.example.mk.webservis_4;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.util.Log;
import android.widget.TextView;

import com.example.mk.webservis_4.Models.Result;
import com.example.mk.webservis_4.RestApi.ManagerAll;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Main2Activity extends AppCompatActivity {
    String id, postid;
    TextView postidTextView, idTextView, nameTextView, bodyTextView, emailTextView;
    List<Result> liste;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        tanimla();
        al();
        istek();
    }

    public void al() {
        Bundle bundle = getIntent().getExtras();
        id = bundle.getString("id");
        postid = bundle.getString("post_id");


    }

    public void tanimla() {
        idTextView = (TextView) findViewById(R.id.id2);
        postidTextView = (TextView) findViewById(R.id.postId2);
        nameTextView = (TextView) findViewById(R.id.name2);
        bodyTextView = (TextView) findViewById(R.id.body2);
        emailTextView = (TextView) findViewById(R.id.email2);

    }

    public void atama(List<Result> list) {
        idTextView.setText("" + list.get(0).getId());
        postidTextView.setText("" + list.get(0).getPostId());
        nameTextView.setText(list.get(0).getName());
        bodyTextView.setText(list.get(0).getBody());
        emailTextView.setText(list.get(0).getEmail());
    }


    public void istek() {
        liste = new ArrayList<>();
        Call<List<Result>> call = ManagerAll.getIntance().managerGetResult(postid, id);
        call.enqueue(new Callback<List<Result>>() {
            @Override
            public void onResponse(Call<List<Result>> call, Response<List<Result>> response) {
                if (response.isSuccessful()) {
                    liste = response.body();
                    atama(liste);
                }
            }

            @Override
            public void onFailure(Call<List<Result>> call, Throwable t) {

            }
        });
    }
}
