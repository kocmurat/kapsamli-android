package com.example.mk.webservis_4.RestApi;

import com.example.mk.webservis_4.Models.Bilgi;
import com.example.mk.webservis_4.Models.Result;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by mk on 04.01.2018.
 */

public interface RestApi {
    @GET("/comments")
    Call<List<Bilgi>> getir();

    @GET("/comments")
    Call<List<Result>> getirResult(@Query("postId") String postid, @Query("id") String id);
}
