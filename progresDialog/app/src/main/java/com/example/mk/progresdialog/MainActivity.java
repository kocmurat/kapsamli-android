package com.example.mk.progresdialog;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        dialogVer();

    }

    public void dialogVer()
    {
    final ProgressDialog progressDialog = new ProgressDialog(this);
    progressDialog.setTitle("Mesajlar Bölümü");
    progressDialog.setMessage("Mesajlar Listeleniyor , Lütfen Bekleyin ! ");
    progressDialog.setCancelable(false);// iptal edilebilirliği kapatıyor , sebebi bir dialogu arka planda işlemler bitene kadar göstermek istiyoruz .
    progressDialog.show();
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {

                    Thread.sleep(20000);
                }catch (Exception e )
                {
                    e.printStackTrace();
                }
                progressDialog.cancel();
            }
        }).start();
    }


}
