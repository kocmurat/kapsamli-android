package com.example.mk.firebase7;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MainActivity extends AppCompatActivity {
    EditText isim, yas, numara, takim;
    Button ekle;
    FirebaseDatabase database;
    DatabaseReference ref;
    String mainChild, key;
    RecyclerView listview;
    List<User> list;
    UserAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        tanimla();
        action();
        load();
    }

    public void tanimla() {
        list = new ArrayList<>();
        listview = (RecyclerView) findViewById(R.id.listview);
        listview.setLayoutManager(new GridLayoutManager(getApplicationContext(),2));
        adapter = new UserAdapter(list,getApplicationContext());
        listview.setAdapter(adapter);

        isim = (EditText) findViewById(R.id.isim);
        yas = (EditText) findViewById(R.id.yas);
        numara = (EditText) findViewById(R.id.numara);
        takim = (EditText) findViewById(R.id.takim);
        ekle = (Button) findViewById(R.id.ekle);
        database = FirebaseDatabase.getInstance();
        ref = database.getReference();
        mainChild = "Users/";


    }

    public void action() {
        ekle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatabaseReference ref2 = ref.child("Users").push();
                key = ref2.getKey();
                Map mp = setMessage(isim.getText().toString(), yas.getText().toString(), numara.getText().toString(), takim.getText().toString());
                Map mp2 = new HashMap();
                mp2.put(mainChild + key, mp);
                ref.updateChildren(mp2, new DatabaseReference.CompletionListener() {
                    @Override
                    public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                        isim.setText("");
                        yas.setText("");
                        numara.setText("");
                        takim.setText("");
                        Toast.makeText(getApplicationContext(), "Kayıt Başarıyla Eklendi", Toast.LENGTH_LONG).show();
                    }
                });
            }
        });
    }

    public Map setMessage(String isim, String yas, String numara, String takim) {
        Map map = new HashMap();
        map.put("isim", isim);
        map.put("yas", yas);
        map.put("numara", numara);
        map.put("takim", takim);
        return map;
    }

    public void load()
    {
        ref.child("Users").addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {

                User u = dataSnapshot.getValue(User.class);
                adapter.notifyDataSetChanged();
                list.add(u);
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
}
