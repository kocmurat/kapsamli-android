package com.example.mk.firebase7;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

/**
 * Created by mk on 02.04.2018.
 */

public class UserAdapter extends RecyclerView.Adapter<UserAdapter.ViewHolder> {

    public List<User> list;
    public Context context;

    public UserAdapter(List<User> list, Context context) {
        this.list = list;
        this.context = context;
    }

    @Override
    public UserAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.luserlayout,parent,false);
        return new  UserAdapter.ViewHolder(view);
    }


    @Override
    public void onBindViewHolder(final UserAdapter.ViewHolder holder, int position) {

        holder.isimText.setText(list.get(position).isim.toString());
        holder.numaraText.setText(list.get(position).numara);
        holder.takimText.setText(list.get(position).takim);
        holder.yasText.setText(list.get(position).yas.toString());
        holder.analayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context," İsim : "+holder.isimText.getText().toString()+" Yaş : "+holder.yasText.getText().toString()+" Takım : "+holder.takimText.getText().toString()+" Numara : "+holder.numaraText.getText().toString(),Toast.LENGTH_LONG).show();

            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }



    public class ViewHolder extends RecyclerView.ViewHolder
    {

        TextView isimText,yasText,numaraText,takimText;
        LinearLayout analayout;
        // recylerview a ait layout un viewlarının tanımlanması işlemleri yapılıyordu.

        public ViewHolder(View itemView) {
            super(itemView);
            isimText = itemView.findViewById(R.id.isimText);
            yasText = itemView.findViewById(R.id.yasText);
            numaraText = itemView.findViewById(R.id.numaraText);
            takimText = itemView.findViewById(R.id.takimText);
            analayout = itemView.findViewById(R.id.analayout);
        }
    }
}
