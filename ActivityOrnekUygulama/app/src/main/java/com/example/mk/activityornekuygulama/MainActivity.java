package com.example.mk.activityornekuygulama;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    List<model> list;
    ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        listeOlustur();

        gec();
    }

    public void listeOlustur() {
        list = new ArrayList<>();
        model m1 = new model("murat", "koç", "0000000");
        model m2 = new model("Furkan", "Tazegüllü", "1111111");
        model m3 = new model("serkan", "taş", "2222222");
        model m4 = new model("oguzhan", "ozkaya", "3333333333");
        list.add(m1);
        list.add(m2);
        list.add(m3);
        list.add(m4);


    }

    public void gec() {
        listView = (ListView) findViewById(R.id.list_view);
        adapterModel adp = new adapterModel(list, this, this);
        listView.setAdapter(adp);
    }

}
