package com.example.mk.activityornekuygulama;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class Main2Activity extends AppCompatActivity {

    TextView textView, textView2, textView3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        tanimla();
        al();
    }

    public void tanimla() {
        textView = (TextView) findViewById(R.id.text1);
        textView2 = (TextView) findViewById(R.id.text2);
        textView3 = (TextView) findViewById(R.id.text3);
    }
    public void al()
    {
        Bundle bundle = getIntent().getExtras();
        String isim = bundle.getString("isim");
        String tel = bundle.getString("tel");
        String soy = bundle.getString("soy");

        textView.setText(isim);
        textView2.setText(tel);
        textView3.setText(soy);
    }
}
