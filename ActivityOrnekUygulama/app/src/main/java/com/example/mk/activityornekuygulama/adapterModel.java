package com.example.mk.activityornekuygulama;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

/**
 * Created by mk on 23.12.2017.
 */

public class adapterModel extends BaseAdapter {

    List<model> list;
    Context context;
    Activity activity;

    public adapterModel(List<model> list, Context context,Activity activity) {
        this.list = list;
        this.context = context;
        this.activity = activity;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = LayoutInflater.from(context).inflate(R.layout.layout, parent, false);
        TextView isim = (TextView) convertView.findViewById(R.id.isim);
        TextView telNo = (TextView) convertView.findViewById(R.id.telNo);
        TextView soyIsim = (TextView) convertView.findViewById(R.id.soyisim);
        LinearLayout linearLayout = convertView.findViewById(R.id.anaLayout);

        final String isimS = list.get(position).getIsim();
        final String soyIsimS = list.get(position).getSoyIsim();
        final String telNos = list.get(position).getNumara();

        isim.setText(isimS);
        telNo.setText(telNos);
        soyIsim.setText(soyIsimS);

        linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent ıntent = new Intent(context, Main2Activity.class);
                ıntent.putExtra("isim", isimS);
                ıntent.putExtra("tel",telNos);
                ıntent.putExtra("soy",soyIsimS);

                activity.startActivity(ıntent);
            }
        });


        return convertView;
    }
}
