package com.example.mk.sendpushnotificationfrommobile.RestApi;





import com.example.mk.sendpushnotificationfrommobile.Result;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by mk on 07.01.2018.
 */

public interface RestApi {
    @FormUrlEncoded
    @POST("/kayitol.php")
    Call<Result> addUser(@Field("kadi") String kullaniciadi, @Field("sifre") String sifre, @Field("mail") String mailadres);

    @FormUrlEncoded
    @POST("/kayitolaktif.php")
    Call<Result> aktifEt(@Field("mail") String mail, @Field("kod") String code);
}
