package com.example.mk.dbtojson.RestApi.RestApi;

import com.example.mk.dbtojson.Models.Kullanici;
import com.example.mk.dbtojson.Models.Result;


import java.util.List;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

/**
 * Created by mk on 07.01.2018.
 */

public interface RestApi {

    @GET("/listele.php")
    Call<List<Kullanici>> listele();

    @FormUrlEncoded
    @POST("/sil.php") Call<Result> sil(@Field("id") String id);



}
