package com.example.mk.dbtojson.RestApi.RestApi;

import com.example.mk.dbtojson.Models.Kullanici;
import com.example.mk.dbtojson.Models.Result;


import java.util.List;

import retrofit2.Call;

/**
 * Created by mk on 07.01.2018.
 */

public class ManagerAll extends BaseManager {

    private static ManagerAll ourInstance = new ManagerAll();

    public static synchronized ManagerAll getInstance() {
        return ourInstance;
    }

    public  Call<List<Kullanici>>  goster() {
        Call<List<Kullanici>>  x = getRestApi().listele();
        return x;
    }

    public Call<Result> deleteFromDb(String id)
    {
        Call<Result> y = getRestApi().sil(id);
        return y;
    }


}
