package com.example.mk.post_2.RestApi;

import com.example.mk.post_2.Models.Sonuc;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by mk on 07.01.2018.
 */

public interface RestApi {
    @FormUrlEncoded
    @POST("/sartliekle.php")
    Call<Sonuc> addUser(@Field("ad") String ad, @Field("soyad") String soyad);
}
