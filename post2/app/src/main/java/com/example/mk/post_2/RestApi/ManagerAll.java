package com.example.mk.post_2.RestApi;


import com.example.mk.post_2.Models.Sonuc;

import retrofit2.Call;

/**
 * Created by mk on 07.01.2018.
 */

public class ManagerAll  extends BaseManager{

    private  static ManagerAll ourInstance = new ManagerAll();

    public  static synchronized ManagerAll getInstance()
    {
        return  ourInstance;
    }

    public Call<Sonuc> ekle(String ad , String soyad)
    {
        Call<Sonuc> x = getRestApi().addUser(ad,soyad);
        return  x ;
    }
}
