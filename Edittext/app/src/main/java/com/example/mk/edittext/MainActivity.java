package com.example.mk.edittext;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    EditText editText;
    TextView textView;
    Button button;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        tanimla();
        tiklama();

    }



    private void tanimla()
    {
        editText =(EditText)findViewById(R.id.text);
        button = (Button) findViewById(R.id.hesaplaButonu);
        textView =(TextView) findViewById(R.id.sonucText);

    }

    private void tiklama()
    {
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String deger = editText.getText().toString();
                int fakHesaplanacakSayi= Integer.parseInt(deger);
                int faktoriyel = hesapla(fakHesaplanacakSayi);

                textView.setText("Sonuç = "+faktoriyel);


            }
        });
    }

    private int hesapla(int sayi)
    {
        int sonuc=1;
        int a=1;
        for(int i=sayi;i>1;i--)
        {
            sonuc = sonuc*i;
        }

        return sonuc;
    }
}
