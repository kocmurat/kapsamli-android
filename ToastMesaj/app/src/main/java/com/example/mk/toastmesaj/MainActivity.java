package com.example.mk.toastmesaj;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    ImageView img ;
    Button btn ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        tanimla();
        islevVer();
        //Toast.makeText(this,"TOAST MESAJI EĞİTİMİ",Toast.LENGTH_LONG).show();

    }

    public void tanimla()
    {
        img = (ImageView) findViewById(R.id.img);
        btn = (Button) findViewById(R.id.btn);

    }
    public void islevVer()
    {

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int rnd = (int) (Math.random()*4+1);
                degister(rnd);

                Toast.makeText(getApplicationContext(),String.valueOf(rnd)+" numaralı resim gösteriliyor",Toast.LENGTH_LONG).show();
            }
        });

    }

    private void degister(int rnd) {

        if ( rnd ==1) {

            img.setImageResource(R.drawable.bir);

        }else if(rnd == 2)
        {

            img.setImageResource(R.drawable.iki);
        }else if(rnd == 3)
        {

            img.setImageResource(R.drawable.uc);
        }else if(rnd == 4)
        {
            
            img.setImageResource(R.drawable.dort);
        }
    }

}
