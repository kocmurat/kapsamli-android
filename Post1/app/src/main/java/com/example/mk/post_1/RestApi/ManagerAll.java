package com.example.mk.post_1.RestApi;

import com.example.mk.post_1.Models.Result;

import retrofit2.Call;

/**
 * Created by mk on 07.01.2018.
 */

public class ManagerAll extends BaseManager {

    private static ManagerAll ourInstance = new ManagerAll();

    public static synchronized ManagerAll getInstance() {
        return ourInstance;
    }

    public Call<Result> ekle(String ad, String soyad) {
        Call<Result> x = getRestApi().addUser(ad, soyad);
        return x;
    }
}
