package com.example.kochan.googlemap3;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;


public class GoogleMapFragment extends Fragment implements OnMapReadyCallback {


    View view;
    MapView  mapView;
    GoogleMap gm;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view =  inflater.inflate(R.layout.fragment_google_map, container, false);
        mapView = (MapView)view.findViewById(R.id.mapview);
        mapView.onCreate(savedInstanceState);
        mapView.onResume();
        mapView.getMapAsync(GoogleMapFragment.this);
        return view ;
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        gm = googleMap;
        LatLng latLng = new LatLng(40.9811118, 28.9930131);
        gm.addMarker(new MarkerOptions().position(latLng).title("Marker in Sydney"));
    }
}
