package com.example.mk.realm_insert_view;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

/**
 * Created by mk on 24.12.2017.
 */

public class adapter extends BaseAdapter {

    List<KisiBilgileri> list;
    Context context ;

    public adapter(List<KisiBilgileri> list, Context context) {
        this.list = list;
        this.context = context;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = LayoutInflater.from(context).inflate(R.layout.layout,parent,false);
        TextView isim = convertView.findViewById(R.id.kullaniciIsim);
        TextView sifre = convertView.findViewById(R.id.kullaniciSifre);
        TextView kullanaciAdi = convertView.findViewById(R.id.kullaniciAdiText);
        TextView cinsiyet = convertView.findViewById(R.id.kullaniciCinsiyet);
        isim.setText(list.get(position).getIsim());
        sifre.setText(list.get(position).getSifre());
        kullanaciAdi.setText(list.get(position).getKullanici());
        cinsiyet.setText(list.get(position).getCinsiyet());

        return convertView;
    }
}
