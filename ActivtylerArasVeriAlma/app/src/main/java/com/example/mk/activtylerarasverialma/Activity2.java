package com.example.mk.activtylerarasverialma;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

public class Activity2 extends AppCompatActivity {

    String kadi;
    String ksifre;
    String kcinsiyet;
    TextView sifre,cinsiyet,adi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_2);
        tanimla();
        al();
    }

    public void al() {
        Bundle ıntent = getIntent().getExtras();
        kadi = ıntent.getString("kullaniciAdi");
        ksifre = ıntent.getString("kullaniciSifre");
        kcinsiyet = ıntent.getString("cinsiyet");

        adi.setText(adi.getText()+ " "+kadi);
        sifre.setText(sifre.getText()+" "+ksifre);
        cinsiyet.setText(cinsiyet.getText()+" "+kcinsiyet);

        Log.i("DEGERLER_2", kadi + " " + ksifre + " " + " " + kcinsiyet);
    }

    public void tanimla() {
        adi = (TextView) findViewById(R.id.isim);
        sifre = (TextView) findViewById(R.id.sifre);
        cinsiyet = (TextView) findViewById(R.id.cinsiyet);

    }
}
