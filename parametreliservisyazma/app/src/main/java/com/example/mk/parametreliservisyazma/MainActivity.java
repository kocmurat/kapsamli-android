package com.example.mk.parametreliservisyazma;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.mk.parametreliservisyazma.Models.Uye;
import com.example.mk.parametreliservisyazma.RestApi.ManagerAll;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    EditText usernameEditText, passwordEditText;
    Button girisYapButon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        tanimla();
        istekAt();
    }

    public void tanimla() {
        usernameEditText = (EditText) findViewById(R.id.userNameEditText);
        passwordEditText = (EditText) findViewById(R.id.passwordEditText);
        girisYapButon = (Button) findViewById(R.id.girisYapButon);
    }

    public void istekAt() {
        girisYapButon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String username = usernameEditText.getText().toString();
                String password = passwordEditText.getText().toString();
                Call<Uye> istek = ManagerAll.getInstance().giris(username, password);
                istek.enqueue(new Callback<Uye>() {
                    @Override
                    public void onResponse(Call<Uye> call, Response<Uye> response) {
                        if (response.isSuccessful()) {
                            String id = response.body().getId();
                            Intent ıntent = new Intent(MainActivity.this,Main2Activity.class);
                            ıntent.putExtra("idDeger",id);
                            startActivity(ıntent);
                        }
                    }

                    @Override
                    public void onFailure(Call<Uye> call, Throwable t) {

                        Toast.makeText(getApplicationContext(),"Bilgilerinizi kontrol ediniz...", Toast.LENGTH_LONG).show();
                    }
                });
            }
        });
    }
}
