package com.example.kochan.googlemap4.RestApi;


import com.example.kochan.googlemap4.MapPojo;

import java.util.List;

import retrofit2.Call;

public class ManagerAll extends BaseManager {


    private static ManagerAll ourInstance = new ManagerAll();

    public static synchronized ManagerAll getInstance() {
        return ourInstance;
    }

    public Call<MapPojo> getMap() {
        Call<MapPojo> x = getRestApi().getMapInformation();
        return x;
    }

}
