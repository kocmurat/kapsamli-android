package com.example.mk.firebase4;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity {

    FirebaseDatabase database;
    DatabaseReference ref1,ref2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        database = FirebaseDatabase.getInstance();
        ref1 = database.getReference("Users/mrt");
        ref2 = database.getReference("Users/ege");

        UserDetails userDetails = new UserDetails( "murat","koc","24");

        ref1.setValue(userDetails);


        Map map = new HashMap();
        map.put("sehir","yozgat");
        map.put("ulke","türkiye");
        map.put("tcno","000000000");
        ref2.setValue(map);

    }


}
