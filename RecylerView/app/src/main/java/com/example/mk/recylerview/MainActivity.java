package com.example.mk.recylerview;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;


public class MainActivity extends AppCompatActivity {
    RecyclerView.LayoutManager layoutManager;
    List<mesajModel> list;
    RecyclerView recylerview;
    adapter adp;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        tanimla();
        listeDoldur();

    }
    public void tanimla()
    {
        recylerview = (RecyclerView) findViewById(R.id.recy);
        layoutManager = new LinearLayoutManager(this);
        recylerview.setLayoutManager(layoutManager);


    }

    public void listeDoldur() {
        list = new ArrayList<>();
        mesajModel m1 = new mesajModel("murat", "merhaba ben murat", R.drawable.murat);
        mesajModel m2 = new mesajModel("mete", "merhaba ben mete", R.drawable.mete);
        mesajModel m3 = new mesajModel("niloya", "merhaba ben niloya", R.drawable.niloya);
        list.add(m1);
        list.add(m2);
        list.add(m3);
        adp = new adapter(this,list);
        recylerview.setAdapter(adp);

    }
}
