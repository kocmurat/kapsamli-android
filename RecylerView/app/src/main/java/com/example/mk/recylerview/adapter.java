package com.example.mk.recylerview;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

/**
 * Created by mk on 19.12.2017.
 */

public class adapter extends RecyclerView.Adapter<adapter.tanimla> {

    Context context;
    List<mesajModel> list;

    public adapter(Context context, List<mesajModel> list) {
        this.context = context;
        this.list = list;
    }


    @Override
    public tanimla onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.layout,parent,false);
        return new tanimla(view);
    }

    @Override
    public void onBindViewHolder(tanimla holder, final int position) {
        holder.mesaj.setText(list.get(position).getMesaj());
        holder.kisi.setText(list.get(position).getIsim());
        holder.img.setImageResource(list.get(position).getResimId());
        holder.img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context,list.get(position).getIsim(),Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public  class tanimla extends RecyclerView.ViewHolder
    {
        ImageView img;
        TextView kisi;
        TextView mesaj;

        public tanimla(View itemView) {
            super(itemView);
            img = (ImageView) itemView.findViewById(R.id.resim);
            kisi = (TextView) itemView.findViewById(R.id.kisi);
            mesaj = (TextView) itemView.findViewById(R.id.mesaj);

        }
    }

}
