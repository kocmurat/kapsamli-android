package com.example.mk.butonlaregitimi;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    Button btn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tanimla();
        islevVer();


    }

    private void tanimla()
    {
        btn = (Button) findViewById(R.id.buton1);


    }



    private void islevVer()
    {
        btn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                System.out.println("Buton tıklandı");

            }
        });

    }


}
