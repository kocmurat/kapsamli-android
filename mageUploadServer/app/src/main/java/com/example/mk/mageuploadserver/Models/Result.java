package com.example.mk.mageuploadserver.Models;

/**
 * Created by mk on 21.01.2018.
 */

public class Result {
    private  String result;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }


    @Override
    public String toString() {
        return "Result{" +
                "result='" + result + '\'' +
                '}';
    }
}
