package com.example.mk.mageuploadserver.RestApi;

import com.example.mk.mageuploadserver.Models.Result;

import retrofit2.Call;

/**
 * Created by mk on 21.01.2018.
 */

public class ManagerAll extends BaseManager {

    private static ManagerAll ourInstance = new ManagerAll();

    public static synchronized ManagerAll getInstance() {
        return ourInstance;
    }

    public Call<Result> gonder(String baslik, String resim) {
        Call<Result> x = getRestApi().gonder(baslik, resim);
        return x;
    }


}
